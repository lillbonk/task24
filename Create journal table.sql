create table journal (
journal_id int primary key auto_increment not null,
name varchar(120) not null,
created_at datetime not null default now(),
update_at datetime default null
)