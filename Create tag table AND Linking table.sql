create table tag (
tag_id int primary key auto_increment not null,
name varchar(128) not null,
created_at datetime not null default now(),
update_at datetime default null
);

-- Linkning table
create table journal_tag (
id int primary key auto_increment not null,
tag_id int not null,
journal_entry_id int not null,
FOREIGN KEY(tag_id) REFERENCES tag(tag_id),
foreign key(journal_entry_id) references journal_entry(journal_entry_id)
)
