
-- Update author
update author set email = 'arnold@schwarzenegger.net' where author.id = 3;
update author set email = 'lillbonk@hotmail.com' where author.id = 4;
update author set last_name = 'Stallooooone' where author.id = 2;

-- update tags
update tag set name = 'Work' where tag_id = 1;
update tag set name = 'JavaScript' where tag_id = 2;
update tag set name = 'CSS/SCSS' where tag_id = 3;

-- update journal
update journal set text = 'Education' where journal_id = 1;
update journal set text = 'Work/Programming' where journal_id = 2;
update journal set text = 'Top Secret' where journal_id = 3;

-- update journal entry
update journal_entry set text = 'GET TO DA CHOPPAAAA!!!!!!' where journal_entry_id = 1;
update journal_entry set text = 'AAAADRIAAAAAN!!!!' where journal_entry_id = 2;
update journal_entry set title = 'My thoughts:' where journal_entry_id = 3;