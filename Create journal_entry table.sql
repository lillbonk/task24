create table journal_entry (
journal_entry_id int primary key auto_increment not null,
title varchar(120),
text text,
created_at datetime not null default now(),
update_at datetime default null,
author_id int not null,
journal_id int not null,
FOREIGN KEY(author_id) REFERENCES author(id),
foreign key(journal_id) references journal(journal_id)
)