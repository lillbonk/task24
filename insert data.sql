-- Insert author
insert into author (first_name, last_name, email) values ('Bruce', 'Willis', 'Bruce@willis.net');
insert into author (first_name, last_name, email) values ('Sylvester', 'Stallone', 'Sylvester@stallone.net');
insert into author (first_name, last_name, email) values ('Arnold', 'Schwarzenegger', 'Arnold@terminator.net');
insert into author (first_name, last_name, email) values ('Robert', 'Björck', 'lillbonk@gmail.com');

select * from author;

-- insert journal entry
insert into journal_entry (title, text, author_id, journal_id) values ('Quotes', 'Get to the choppa', 3,4 );
insert into journal_entry (title, text, author_id, journal_id) values ('Quotes', 'Aaaaaaaadrian!!!', 2,4 );
insert into journal_entry (title, text, author_id, journal_id) values ('State of mind', 'Feel like im getting a hang of things!', 4,2 );

select * from journal_entry;

-- insert tag to journal_tag (linking table), connecting via journal_entry_id and tag_id
insert into journal_tag (tag_id, journal_entry_id) values (5,1);
insert into journal_tag (tag_id, journal_entry_id) values (5,2);
insert into journal_tag (tag_id, journal_entry_id) values (5,3);
insert into journal_tag (tag_id, journal_entry_id) values (6,3);

select * from journal_tag;

-- Insert journal names to journal table
insert into journal (name) values ('Edu');
insert into journal (name) values ('Work');
insert into journal (name) values ('Private');
insert into journal (name) values ('Fun');

select* from journal;

-- Insert tag names to tag table
insert into tag (name) values ('HTML');
insert into tag (name) values ('JS');
insert into tag (name) values ('CSS');
insert into tag (name) values ('Node');
insert into tag (name) values('Fun');
insert into tag (name) values('Private');
insert into tag (name) values ('Angular,');
insert into tag (name) values ('React');


