-- SELECT Journal entries with tags and author
SELECT je.title, je.text, je.created_at, CONCAT(a.first_name, ' ', a.last_name) AS author, GROUP_CONCAT(t.name) AS tags
FROM journal_entry AS je
LEFT JOIN journal_tag AS jt using (journal_entry_id)
LEFT JOIN tag AS t using (tag_id)
INNER JOIN author a ON je.author_id = a.id
GROUP BY je.journal_entry_id;

--  select author + title+text
SELECT CONCAT(author.first_name, ' ', author.last_name) AS author, journal_entry.title, journal_entry.text 
FROM author
LEFT JOIN journal_entry on author.id = journal_entry.author_id;

