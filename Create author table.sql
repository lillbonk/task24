create table author (
id int primary key auto_increment not null,
first_name varchar(80) not null,
last_name varchar(120) not null,
email varchar(150),
created_at datetime not null default now(),
update_at datetime default null
);